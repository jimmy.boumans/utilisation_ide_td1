package dto;

public class CompteEpargne extends Compte {

	// Attributs
	private Double taux;

	// Constructeurs
	public CompteEpargne() {
		super();
	}

	public CompteEpargne(Double taux) {
		super();
		this.taux = taux;
	}

	// Getter & Setter
	public Double getTaux() {
		return taux;
	}

	public void setTaux(Double taux) {
		this.taux = taux;
	}

	// Methodes
	@Override
	public String toString() {
		return "CompteEpargne [taux=" + taux + "]";
	}

}
