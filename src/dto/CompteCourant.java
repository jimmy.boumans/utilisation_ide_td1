package dto;

public class CompteCourant extends Compte {

	// Attributs
	private Double decouvert;

	// Constructeurs
	public CompteCourant() {
		super();
	}

	public CompteCourant(Double decouvert) {
		super();
		this.decouvert = decouvert;
	}

	// Getter & Setter
	public Double getDecouvert() {
		return decouvert;
	}

	public void setDecouvert(Double decouvert) {
		this.decouvert = decouvert;
	}

	// Methodes
	@Override
	public String toString() {
		return "CompteCourant [decouvert=" + decouvert + "]";
	}

}
