package dto;

import java.time.LocalDate;

public class Compte {

	// Attributs
	protected Integer codeCompte;
	protected LocalDate dateCreation;
	protected Double solde;
	protected Integer codeClient;

	// Constructeurs
	public Compte() {
		super();
	}

	public Compte(Integer codeCompte, LocalDate dateCreation, Double solde, Integer codeClient) {
		super();
		this.codeCompte = codeCompte;
		this.dateCreation = dateCreation;
		this.solde = solde;
		this.codeClient = codeClient;
	}

	// Getter & Setter
	public Integer getCodeCompte() {
		return codeCompte;
	}

	public void setCodeCompte(Integer codeCompte) {
		this.codeCompte = codeCompte;
	}

	public LocalDate getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(LocalDate dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Double getSolde() {
		return solde;
	}

	public void setSolde(Double solde) {
		this.solde = solde;
	}

	public Integer getCodeClient() {
		return codeClient;
	}

	public void setCodeClient(Integer codeClient) {
		this.codeClient = codeClient;
	}

	// M�thodes
	@Override
	public String toString() {
		return "Compte [codeCompte=" + codeCompte + ", dateCreation=" + dateCreation + ", solde=" + solde
				+ ", codeClient=" + codeClient + "]";
	}

}
