package dto;

public class Client {

	// Attributs
	private Integer code;
	private String nom;
	private String email;
	private Integer codeCompte;

	// Constructeur
	public Client() {
		super();
	}

	public Client(Integer code, String nom, String email, Integer codeCompte) {
		super();
		this.code = code;
		this.nom = nom;
		this.email = email;
		this.codeCompte = codeCompte;
	}

	// Getter & Setter
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getCodeCompte() {
		return codeCompte;
	}

	public void setCodeCompte(Integer codeCompte) {
		this.codeCompte = codeCompte;
	}

	// Methodes
	@Override
	public String toString() {
		return "Client [code=" + code + ", nom=" + nom + ", email=" + email + ", codeCompte=" + codeCompte + "]";
	}

}
