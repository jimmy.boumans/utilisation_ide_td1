package service;

import java.time.LocalDate;
import java.util.ArrayList;

import dto.Client;
import dto.Compte;

public class Operation {

	// Attributs
	private ArrayList<Client> clients;
	private ArrayList<Compte> comptes;
	private LocalDate date = LocalDate.now();

	// Constructeur
	public Operation() {
		super();
	}

	public Operation(ArrayList<Client> clients, ArrayList<Compte> comptes, LocalDate date) {
		super();
		this.clients = clients;
		this.comptes = comptes;
		this.date = date;
	}

	// Getter & Setter
	public ArrayList<Client> getClients() {
		return clients;
	}

	public void setClients(ArrayList<Client> clients) {
		this.clients = clients;
	}

	public ArrayList<Compte> getComptes() {
		return comptes;
	}

	public void setComptes(ArrayList<Compte> comptes) {
		this.comptes = comptes;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	// M�thodes
	@Override
	public String toString() {
		return "Operation [clients=" + clients + ", comptes=" + comptes + ", date=" + date + "]";
	}

	public void initClientCompte() {
		if (clients == null || clients.isEmpty()) {
			clients.add(new Client(1, "Toto", "mail.toto@epsi.fr", 1));
			clients.add(new Client(2, "Titi", "mail.titi@epsi.fr", 2));
			clients.add(new Client(3, "Tata", "mail.tata@epsi.fr", 3));
			clients.add(new Client(4, "Tutu", "mail.tutu@epsi.fr", 4));
		}
		if (comptes == null || comptes.isEmpty()) {
			comptes.add(new Compte(1, date, 2000d, 1));
			comptes.add(new Compte(2, date, 1000d, 2));
			comptes.add(new Compte(3, date, 500d, 3));
			comptes.add(new Compte(4, date, 3000d, 4));
		}
	}


}
